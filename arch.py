#Jacob Jones, JD Martin, Vivek Patel

import re

userfile = input('select a file\n')
# Using readlines() 
file1 = open(userfile, 'r') 
Lines = file1.readlines()
f = open("code.txt", "w")
f2 = open("regs.txt","w")


#count = 0
# Strips the newline character
X = []
for i in range(0,31):
    X.append(0)

DMEM = []
for i in range(0,8):
    DMEM.append(0)




for line in Lines:
    print(line.strip())
    if re.match('ADD ', line.strip()):
        op = '10001011000'
        a = re.findall('\d{1,2}|XZR',line.strip())
        b = "{0:b}".format(int(a[0]))
        rdv = int(a[0])
        rd = b.zfill(5)
        if a[1] == 'XZR':
            rn = '00000'
            rnv = 0
        else:
            b = "{0:b}".format(int(a[1]))
            rn = b.zfill(5)
            rnv = int(a[1])
        if a[2] == 'XZR':
            rm = '00000'
            rmv = 0
        else:
            b = "{0:b}".format(int(a[2]))
            rm = b.zfill(5)
            rmv = int(a[2])
        ins = op + rm + '000000' + rn +rd
        X[rdv] = X[rmv] + X[rnv]
        print(ins)
        print(X)
    elif re.match('SUB ', line.strip()):
        op = '11001011000'
        a = re.findall('\d{1,2}|XZR',line.strip())
        b = "{0:b}".format(int(a[0]))
        rdv = int(a[0])
        rd = b.zfill(5)
        if a[1] == 'XZR':
            rn = '00000'
            rnv = 0
        else:
            b = "{0:b}".format(int(a[1]))
            rn = b.zfill(5)
            rnv = int(a[1])
        if a[2] == 'XZR':
            rm = '00000'
            rmv = 0
        else:
            b = "{0:b}".format(int(a[2]))
            rm = b.zfill(5)
            rmv = int(a[2])
        ins = op + rm + '000000' + rn +rd
        X[rdv] = X[rmv] - X[rnv]
        print(X)
        print(ins)
    elif re.match('AND ', line.strip()):
        op = '10001010000'
        a = re.findall('\d{1,2}|XZR',line.strip())
        b = "{0:b}".format(int(a[0]))
        rd = b.zfill(5)
        b = "{0:b}".format(int(a[1]))
        rn = b.zfill(5)
        b = "{0:b}".format(int(a[2]))
        rm = b.zfill(5)
        ins = op + rm + '000000' + rn +rd
        print(ins)  
    elif re.match('ORR ', line.strip()):
        op = '10101010000'
        a = re.findall('\d{1,2}|XZR',line.strip())
        b = "{0:b}".format(int(a[0]))
        rd = b.zfill(5)
        b = "{0:b}".format(int(a[1]))
        rn = b.zfill(5)
        b = "{0:b}".format(int(a[2]))
        rm = b.zfill(5)
        ins = op + rm + '000000' + rn +rd
        print(ins)
    elif re.match('EOR ', line.strip()):
        op = '11101010000'
        a = re.findall('\d{1,2}|XZR',line.strip())
        b = "{0:b}".format(int(a[0]))
        rd = b.zfill(5)
        b = "{0:b}".format(int(a[1]))
        rn = b.zfill(5)
        b = "{0:b}".format(int(a[2]))
        rm = b.zfill(5)
        ins = op + rm + '000000' + rn +rd
        print(ins)
    elif re.match('LSL ', line.strip()):
        op = '11010011011'
        a = re.findall('\d{1,2}|XZR',line.strip())
        b = "{0:b}".format(int(a[0]))
        rd = b.zfill(5)
        b = "{0:b}".format(int(a[1]))
        rn = b.zfill(5)
        b = "{0:b}".format(int(a[2]))
        rm = b.zfill(5)
        ins = op + rm + '000000' + rn +rd
        print(ins)
    elif re.match('LSR ', line.strip()):
        op = '11010011010'
        a = re.findall('\d{1,2}|XZR',line.strip())
        b = "{0:b}".format(int(a[0]))
        rd = b.zfill(5)
        b = "{0:b}".format(int(a[1]))
        rn = b.zfill(5)
        b = "{0:b}".format(int(a[2]))
        rm = b.zfill(5)
        ins = op + rm + '000000' + rn +rd
        print(ins)
    elif re.match('ADDI ', line.strip()):
        op = '1001001000'
        a = re.findall('\d{1,2}|XZR',line.strip())
        print(a)
        b = "{0:b}".format(int(a[0]))
        rd = b.zfill(5)
        rdv = int(a[0])
        if a[1] == 'XZR':
            rn = '00000'
            rnv = 0
        else:
            b = "{0:b}".format(int(a[1]))
            rn = b.zfill(5)
            rnv = int(a[1])
                
        b = "{0:b}".format(int(a[2]))
        imm = b.zfill(12)
        immv = int(a[2])
        
        X[rdv] = rnv + immv
        print(X)
        ins = op + imm + rn + rd
    elif re.match('SUBI ', line.strip()):
        op = '1011001000'
        a = re.findall('\d{1,2}|XZR',line.strip())
        b = "{0:b}".format(int(a[0]))
        rd = b.zfill(5)
        rdv = int(a[0])
        if a[1] == 'XZR':
            rn = '00000'
            rnv = 0
        else:
            rnv = int(a[1])
            b = "{0:b}".format(int(a[1]))
            rn = b.zfill(5)
        b = "{0:b}".format(int(a[2]))
        immv = int(a[2])
        imm = b.zfill(12)
        ins = op + imm + rn + rd
        print(ins)
    elif re.match('ORRI ', line.strip()):
        op = '1011001000'
        a = re.findall('\d{1,2}|XZR',line.strip())
        b = "{0:b}".format(int(a[0]))
        rd = b.zfill(5)
        if a[1] == 'XZR':
            rn = '00000'
        else:
            b = "{0:b}".format(int(a[1]))
            rn = b.zfill(5)
        b = "{0:b}".format(int(a[2]))
        imm = b.zfill(12)
        ins = op + imm + rn + rd
        print(ins)
    elif re.match('EORI ', line.strip()):
        op = '1101001000'
        a = re.findall('\d{1,2}#\d{1,2}|XZR',line.strip())
        b = "{0:b}".format(int(a[0]))
        rd = b.zfill(5)
        if a[1] == 'XZR':
            rn = '00000'
        else:
            b = "{0:b}".format(int(a[1]))
            rn = b.zfill(5)
        b = "{0:b}".format(int(a[2]))
        imm = b.zfill(12)
        ins = op + imm + rn + rd
        print(ins)
    elif re.match('SUBS ', line.strip()):
        op = '1111000100'
        a = re.findall('\d{1,2}#\d{1,2}|XZR',line.strip())
        b = "{0:b}".format(int(a[0]))
        rd = b.zfill(5)
        if a[1] == 'XZR':
            rn = '00000'
        else:
            b = "{0:b}".format(int(a[1]))
            rn = b.zfill(5)
        b = "{0:b}".format(int(a[2]))
        rm = b.zfill(5)
        ins = op + rm + '000000' + rn +rd
        print(ins)
    elif re.match('SUBIS ', line.strip()):
        op = '1111000100'
        a = re.findall('\d{1,2}#\d{1,2}|XZR',line.strip())
        b = "{0:b}".format(int(a[0]))
        rd = b.zfill(5)
        if a[1] == 'XZR':
            rn = '00000'
        else:
            b = "{0:b}".format(int(a[1]))
            rn = b.zfill(5)
        
        b = "{0:b}".format(int(a[2]))
        imm = b.zfill(12)
        immv = int(a[2])
        ins = op + imm + rn + rd
        X[rd]= X[rnv] - immv
        print(X)
        print(ins)
    elif re.match('LDUR ', line.strip()):
        op = '1011001000'
        a = re.findall('\d{1,2}#\d{1,2}|XZR',line.strip())
        b = "{0:b}".format(int(a[0]))
        rt = b.zfill(5)
        if a[1] == 'XZR':
	        rn = '00000'
        else:
            b = "{0:b}".format(int(a[1]))
            rn = b.zfill(5)
        b = "{0:b}".format(int(a[2]))
        ad = b.zfill(12)
        ins = op + ad + '00' +rn + rt
        print(ins)
    elif re.match('STUR ', line.strip()):
        op = '11111000000'
        a = re.findall('\d{1,2}#\d{1,2}|XZR',line.strip())
        b = "{0:b}".format(int(a[0]))
        rt = b.zfill(5)
        if a[1] == 'XZR':
            rn = '0000'
        else:
            b = "{0:b}".format(int(a[1]))
            rn = b.zfill(5)
            b = "{0:b}".format(int(a[2]))
            ad = b.zfill(12)
        ins = op + ad + '00' +rn + rt
        print(ins)
    elif re.match('CBZ ', line.strip()):
        op = '10110100'
        a = re.findall('\d{1,2}#\d{1,2}|XZR',line.strip())
        b = "{0:b}".format(int(a[0]))
        rt = b.zfill(5)
        ins = op + '0000000000000000000' + rt
        print(ins)
    elif re.match('CBNZ ', line.strip()):
        op = '10110101'
        a = re.findall('\d{1,2}#\d{1,2}|XZR',line.strip())
        b = "{0:b}".format(int(a[0]))
        rt = b.zfill(5)
        ins = op + '0000000000000000000' + rt
        print(ins)
    elif re.match('B ', line.strip()):
        op = '000101'
        ins = op + '0000000000000000000000000000'
        print(ins)
    elif re.match('B.', line.strip()):
        op = '01010100'
        ins = op + '0000000000000000000000000000'
        print(ins)
    





    f.write(ins + '\n')
j = 0
for i in X:
    f2.write("X"+str(j)+" = "+str(i)+"\n")
    j = j+1
f.close()



